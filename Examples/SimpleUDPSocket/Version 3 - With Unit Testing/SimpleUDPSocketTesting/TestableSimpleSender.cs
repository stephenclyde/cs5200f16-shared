﻿using SimpleUDPSocket;

namespace SimpleUDPSocketTesting
{
    internal class TestableSimpleSender : SimpleSender
    {
        public new static int GetNextId()
        {
            return SimpleSender.GetNextId(); 
        }

        public new int SendToPeers(string message)
        {
            return base.SendToPeers(message);
        }

        public static void SetNextId(int id)
        {
            NextId = id;
        }
    }
}
