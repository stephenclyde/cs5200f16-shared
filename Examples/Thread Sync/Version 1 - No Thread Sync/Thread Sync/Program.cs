﻿using System;

namespace Thread_Sync
{
    public class Program
    {
        static void Main(string[] args)
        {
            Worker workerA = new Worker() { Name = "A"};
            workerA.Start();

            Worker workerB = new Worker() { Name = "B"};
            workerB.Start();

            Worker workerC = new Worker() { Name = "C"};
            workerC.Start();

            bool keepGoing = true;
            while (keepGoing)
            {
                ConsoleKeyInfo input = Console.ReadKey(true);
                keepGoing = (input.Key != ConsoleKey.X);
            }

            workerA.Stop();
            workerB.Stop();
            workerC.Stop();
        }
    }
}
