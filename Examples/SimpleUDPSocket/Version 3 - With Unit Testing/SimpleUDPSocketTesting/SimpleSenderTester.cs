﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using SimpleUDPSocket;

namespace SimpleUDPSocketTesting
{
    [TestClass]
    public class SimpleSenderTester
    {
        private static readonly object MyLock = new object();

        [TestMethod]
        public void SimpleSenderTester_TestSingleGetNextId()
        {
            lock (MyLock)
            {
                int id1 = TestableSimpleSender.GetNextId();
                int id2 = TestableSimpleSender.GetNextId();
                if (id1 != int.MaxValue)
                    Assert.IsTrue(id2 == id1 + 1);
                else
                    Assert.AreEqual(1, id2);
            }
        }

        [TestMethod]
        public void SimpleSenderTester_TestLotsOfGetNextId()
        {
            Parallel.For(0, 100000, (i) =>
            {
                lock (MyLock)  // Without this lock here and around other test cases that use
                               // GetNextId, the id2 == id1 + 1 assertation may fail
                {
                    int id1 = TestableSimpleSender.GetNextId();
                    int id2 = TestableSimpleSender.GetNextId();
                    if (id1 != int.MaxValue)
                        Assert.IsTrue(id2 == id1 + 1);
                    else
                        Assert.AreEqual(1, id2);
                }
            });
        }

        [TestMethod]
        public void SimpleSenderTester_TestRolloverOfGetNextId()
        {
            lock (MyLock)
            {
                TestableSimpleSender.SetNextId(int.MaxValue);
                int id1 = TestableSimpleSender.GetNextId();
                Assert.AreEqual(1, id1);
            }
        }

        [TestMethod]
        public void SimpleSenderTester_TestSendToPeers()
        {
            TestableSimpleSender sender = new TestableSimpleSender();
            List<UdpClient> clients = new List<UdpClient>();

            for (int i = 0; i < 100; i++)
            {
                UdpClient client = CreateReceiver();
                clients.Add(client);
                sender.Peers.Add(GetLocalIpEndPoint(client));
            }

            DateTime beginTime = DateTime.Now;

            int messageId = sender.SendToPeers("Test message");
            Assert.AreNotEqual(-1, messageId);

            Parallel.ForEach(clients, (client) =>
            {
                Message incomingMessage = ReceiveMessage(client);
                Assert.IsNotNull(incomingMessage);
                Assert.AreEqual(messageId, incomingMessage.Id);
                Assert.IsTrue(beginTime < incomingMessage.Timestamp);
                Assert.AreEqual("Test message", incomingMessage.Text);
            });
        }

        private UdpClient CreateReceiver()
        {
            IPEndPoint localEp = new IPEndPoint(IPAddress.Any, 0);
            UdpClient client = new UdpClient(localEp) {Client = {ReceiveTimeout = 5000}};

            return client;
        }

        private IPEndPoint GetLocalIpEndPoint(UdpClient client)
        {
            return new IPEndPoint(IPAddress.Loopback, ((IPEndPoint) client.Client.LocalEndPoint).Port);
        }

        private Message ReceiveMessage(UdpClient client)
        {
            IPEndPoint remoteEp = new IPEndPoint(IPAddress.Any, 0);
            byte[] bytes = null;

            try
            {
                bytes = client.Receive(ref remoteEp);
            }
            catch (SocketException err)
            {
                if (err.SocketErrorCode != SocketError.TimedOut)
                    throw;
            }

            Message result = Message.Decode(bytes);
            return result;
        }

    }
}
