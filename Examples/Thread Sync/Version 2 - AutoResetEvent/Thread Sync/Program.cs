﻿using System;
using System.Threading;

namespace Thread_Sync
{
    public class Program
    {
        static void Main(string[] args)
        {
            AutoResetEvent resetEvent = new AutoResetEvent(false);


            Worker workerA = new Worker() { Name = "A", Flag = resetEvent};
            workerA.Start();

            Worker workerB = new Worker() { Name = "B", Flag = resetEvent };
            workerB.Start();

            Worker workerC = new Worker() { Name = "C", Flag = resetEvent };
            workerC.Start();

            bool keepGoing = true;
            while (keepGoing)
            {
                ConsoleKeyInfo input = Console.ReadKey(true);
                resetEvent.Set();
                keepGoing = (input.Key != ConsoleKey.X);
            }

            workerA.Stop();
            workerB.Stop();
            workerC.Stop();
        }
    }
}
