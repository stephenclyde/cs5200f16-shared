﻿namespace MulticastComm
{
    public class FinalMessage : Message
    {
        public static string EndOfMessagesText = "Over";
        public FinalMessage()
        {
            Text = EndOfMessagesText;
        }
    }
}
