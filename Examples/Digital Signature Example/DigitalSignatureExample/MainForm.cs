﻿using System;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace DigitalSignatureExample
{
    /// <summary>
    /// Note that this class illustrates signing and sign verifiction code for the originator and receiver of a signed message.
    /// It does not illustrate the sharing of the public key or actually sending of the message
    /// </summary>
    public partial class MainForm : Form
    {
        private RSACryptoServiceProvider _rsa;
        private RSAPKCS1SignatureFormatter _rsaSigner;
        private RSAParameters _senderPublicKeyInfo;

        private byte[] _encryptedmessageHash;

        public MainForm()
        {
            InitializeComponent();
        }

        private void generateKeysButton_Click(object sender, EventArgs e)
        {
            // The originating process would include this logic as part of its startup or initialization.  It would also make the publicKey available to other process
            // that will receiving messages (or shared resources) from the orginating process.

            _rsa = new RSACryptoServiceProvider();
            _senderPublicKeyInfo = _rsa.ExportParameters(true);
            publicKey.Text = FormatByteArray(_senderPublicKeyInfo.Exponent) + FormatByteArray(_senderPublicKeyInfo.Modulus);
            privateKey.Text = FormatByteArray(_senderPublicKeyInfo.D) + FormatByteArray(_senderPublicKeyInfo.Modulus);

            // Create a signer that that will create digital signatures using this key (the private part)
            _rsaSigner = new RSAPKCS1SignatureFormatter(_rsa);
            _rsaSigner.SetHashAlgorithm("SHA1");

        }

        private void signButton_Click(object sender, EventArgs e)
        {
            // The originating process would use the kind of logic to sign a message or shared resource

            SHA1Managed hasher = new SHA1Managed();
            byte[] messageBytes = Encoding.Unicode.GetBytes(sampleMessage.Text);
            byte[] messageHash = hasher.ComputeHash(messageBytes);

            hash.Text = FormatByteArray(messageHash);

            _encryptedmessageHash = _rsaSigner.CreateSignature(messageHash);

            encryptedHash.Text = FormatByteArray(_encryptedmessageHash);
        }

        private void verifyButton_Click(object sender, EventArgs e)
        {
            // A receiving process would use this kind of logic to verify the authenticity of the message
            // (or shared resource)
            
            // Step 1 - Get the public key for the sender and set up the sign verifier.  There would be
            //          done by the reciever sometime before receiving messages
            //
            //      a. Setup RSAParameter using public key data (keyInfo.exponent, keyInfo.modulus)
            //      b. Create RSACryptoServiceProviders and import keyInfo

            RSAParameters receiverRsaKeyInfo = new RSAParameters
            {
                Modulus = _senderPublicKeyInfo.Modulus,
                Exponent = _senderPublicKeyInfo.Exponent
            };

            RSACryptoServiceProvider receiverRsa = new RSACryptoServiceProvider();
            receiverRsa.ImportParameters(receiverRsaKeyInfo);

            RSAPKCS1SignatureDeformatter rsaSignComparer = new RSAPKCS1SignatureDeformatter(receiverRsa);
            rsaSignComparer.SetHashAlgorithm("SHA1");

            // Step 2 - Verify the message's signature
            //
            //      a.  Compute the hash for the message, just like the sender did
            //      b.  Verify the signature against the hash
            //        
            SHA1Managed hasher = new SHA1Managed();
            byte[] messageBytes = Encoding.Unicode.GetBytes(sampleMessage.Text);
            byte[] messageHash = hasher.ComputeHash(messageBytes);

            bool isVerified = rsaSignComparer.VerifySignature(messageHash, _encryptedmessageHash);
            verifyResults.Text = isVerified ? @"VERIFIED" : @"NOT VERIFIED";
        }

        /// <summary>
        /// This method simply formats a byte array for display purposes. It is not part of the signing or sign verification logic
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private string FormatByteArray(byte[] data)
        {
            string result = "";
            foreach (byte d in data)
                result += $"{d} ";
            return result;
        }
    }
}
