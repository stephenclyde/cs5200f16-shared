﻿using System;
using System.Collections.Generic;

using System.Net;
using System.Net.Sockets;

using log4net;

namespace SimpleUDPSocket
{
    public class SimpleSender
    {
        protected static readonly ILog Logger = LogManager.GetLogger(typeof(SimpleReceiver));

        protected static int NextId = 1;
        protected readonly UdpClient MyUdpClient;

        public List<IPEndPoint> Peers { get; private set; }

        public SimpleSender()
        {
            IPEndPoint localEp = new IPEndPoint(IPAddress.Any, 0);
            MyUdpClient = new UdpClient(localEp);
            Peers = new List<IPEndPoint>();
        }

        public void SendStuff()
        {
            string cmd = string.Empty;
            while (string.IsNullOrEmpty(cmd) || cmd.Trim().ToUpper() != "EXIT")
            {
                Console.Write("A=Add Peer, S=Send Message, or EXIT: " );
                cmd = Console.ReadLine();
                switch (cmd?.Trim().ToUpper())
                {
                    case "A":
                        AddPeer();
                        break;
                    case "S":
                        AskForMessageAndSend();
                        break;
                    case "EXIT":
                        SendToPeers(cmd);
                        break;
                }
            }
        }

        protected void AddPeer()
        {
            Console.Write("Enter Peer EP (host:port): ");
            string peer = Console.ReadLine();
            if (!string.IsNullOrWhiteSpace(peer))
            {
                IPEndPoint peerAddress = EndPointParser.Parse(peer);
                if (peerAddress != null)
                {
                    Peers.Add(peerAddress);
                    Logger.DebugFormat("Add {0} as a peer", peerAddress);
                }
            }
        }

        private void AskForMessageAndSend()
        {
            Console.Write("Enter a message to send: ");
            string message = Console.ReadLine();
            if (!string.IsNullOrWhiteSpace(message))
                SendToPeers(message);
        }

        protected int SendToPeers(string message)
        {
            int messageId = -1;
            if (Peers != null && Peers.Count > 0)
            {
                messageId = GetNextId();
                Message msg = new Message()
                {
                    Id = messageId,
                    Timestamp = DateTime.Now,
                    Text = message
                };

                byte[] bytes = msg.Encode();

                foreach (IPEndPoint ep in Peers)
                {
                    int bytesSent = MyUdpClient.Send(bytes, bytes.Length, ep);
                    Logger.InfoFormat("Send to {0} was {1}", ep, (bytesSent == bytes.Length) ? "Successful" : "Not Successful");
                }
            }
            return messageId;
        }

        protected static int GetNextId()
        {
            if (NextId == int.MaxValue)
                NextId = 1;
            return NextId++;
        }
    }
}
