﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Thread_Sync
{
    public class Program
    {
        static void Main(string[] args)
        {
            AutoResetEvent resetEvent = new AutoResetEvent(false);

            List<string> myList = new List<string>();

            Worker workerA = new Worker() { Name = "A", Flag = resetEvent, SharedResource = myList};
            workerA.Start();

            Worker workerB = new Worker() { Name = "B", Flag = resetEvent, SharedResource = myList };
            workerB.Start();

            Worker workerC = new Worker() { Name = "C", Flag = resetEvent, SharedResource = myList };
            workerC.Start();

            bool keepGoing = true;
            while (keepGoing)
            {
                ConsoleKeyInfo input = Console.ReadKey(true);
                resetEvent.Set();                            
                keepGoing = (input.Key != ConsoleKey.X);
            }

            workerA.Stop();
            workerB.Stop();
            workerC.Stop();
        }
    }
}
