﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using MulticastComm;

namespace Originator
{
    public class Program
    {
        public static void Main(string[] args)
        {
            OriginatorCommunicator originator = new OriginatorCommunicator()
            {
                GroupAddress = IPAddress.Parse("224.3.2.1"),
                GroupPort = 5000,
                TimeoutInMilliseconds = 1000,
                NumberOfResponsesToWaitFor = 1,
                MessagesToSend = new List<Message>()
                {
                    new Message() {Text = "Hello Target"},
                    new Message() {Text = "Have a nice day"},
                    new FinalMessage()

                },
                DisplayMethod = ConsoleDisplay
            };

            originator.Start();

            Console.ReadKey();

            originator.Stop();
        }

        public static void ConsoleDisplay(string text)
        {
            Console.WriteLine(text);
        }
    }
}
