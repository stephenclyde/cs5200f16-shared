﻿using System;
using System.Threading;

namespace Thread_Sync
{
    public class Worker
    {
        private bool _keepGoing;
        private Thread _myThead;

        public string Name { get; set; }
        public AutoResetEvent Flag { get; set; }

        public void Start()
        {
            _keepGoing = true;
            _myThead = new Thread(Run);
            _myThead.Start();
        }

        public void Stop()
        {
            _keepGoing = false;
        }

        public void Run()
        {
            while (_keepGoing)
            {
                if (Flag.WaitOne(100))
                    Console.WriteLine($"{Name} is doing something");
            }
        }

    }
}
