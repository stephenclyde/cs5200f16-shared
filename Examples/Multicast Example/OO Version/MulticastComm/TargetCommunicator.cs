﻿using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace MulticastComm
{
    public class TargetCommunicator : MulticastCommunicator
    {
        public List<Message>    ReplyMessages { get; set; }

        protected override void SetupUdpClient()
        {
            // Create a UDP Client and have it bound to any available port
            MyUdpClient = new UdpClient(GroupPort) { Client = { ReceiveTimeout = TimeoutInMilliseconds } };
            MyUdpClient.JoinMulticastGroup(GroupAddress);
        }

        protected override void DoStuff()
        {
            IPEndPoint senderEndPoint = Receive(1);
            Send(ReplyMessages, senderEndPoint);
        }

        protected override void TearDownClient()
        {
            MyUdpClient.DropMulticastGroup(GroupAddress);
            MyUdpClient.Close();
        }

    }
}
