﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Net;
using System.Text;
using log4net;

namespace SimpleUDPSocket
{
    [DataContract]
    public class Message
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof (Message));

        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public DateTime Timestamp { get; set; }
        [DataMember]
        public string Text { get; set; }

        public byte[] Encode()
        {
            Logger.Debug("Encode message");

            MemoryStream memoryStream = new MemoryStream();

            // Write out Id
            byte[] bytes = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(Id));
            memoryStream.Write(bytes, 0, bytes.Length);

            // Write out DataTime as a long total milliseconds
            bytes = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(Timestamp.Ticks));
            memoryStream.Write(bytes, 0, bytes.Length);

            // Write out Text
            bytes = Encoding.BigEndianUnicode.GetBytes(Text);
            memoryStream.Write(bytes, 0, bytes.Length);

            return memoryStream.ToArray();
        }

        public static Message Decode(byte[] bytes)
        {
            Message message = null;
            if (bytes != null)
            {
                message = new Message();

                message.Id = IPAddress.NetworkToHostOrder(BitConverter.ToInt32(bytes, 0));
                long ticks = IPAddress.NetworkToHostOrder(BitConverter.ToInt64(bytes, 4));
                message.Timestamp = new DateTime(ticks);
                message.Text = Encoding.BigEndianUnicode.GetString(bytes, 12, bytes.Length - 12);
            }

            return message;
        }


    }
}
