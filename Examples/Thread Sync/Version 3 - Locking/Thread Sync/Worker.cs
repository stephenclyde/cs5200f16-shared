﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Thread_Sync
{
    public class Worker
    {
        private static readonly object MyLock = new object();
        private bool _keepGoing;
        private Thread _myThead;

        public string Name { get; set; }
        public AutoResetEvent Flag { get; set; }

        public List<string> SharedResource { get; set; } 

        public void Start()
        {
            _keepGoing = true;
            _myThead = new Thread(Run);
            _myThead.Start();
        }

        public void Stop()
        {
            _keepGoing = false;
        }

        public void Run()
        {
            while (_keepGoing)
            {
                if (Flag.WaitOne(100))
                {
                    Console.WriteLine($"{Name} is doing something");
                    lock (MyLock)
                    {
                        var number = SharedResource.Count + 1;
                        SharedResource.Add($"string #{number} added to shared resource at {DateTime.Now.ToLongTimeString()}");
                    }
                }
            }
        }

    }
}
